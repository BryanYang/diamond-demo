const path = require('path');
const fs = require('fs-extra');
const mkdirp = require('mkdirp');
const rp = require('request-promise');
const io = require('socket.io-client');
const co = require('co');

const CACHE_FILE = './cache/local';
const cachePath = path.resolve(process.cwd() ,CACHE_FILE);
const MERGE_EVENT = 'merge';
const MERGE_INTERVAL = 1000 * 30;

class SDK {
    constructor(){
        this.SOCKET = {
            connect: false,
        };

        this.KEY_MAP = new Map();

         // 订阅字段
        this.subscribe = (key) => {
            this.KEY_MAP.set(key, '');
        }

        // 启动时执行
        this.init = function* (){
            //1. 获取服务端地址缓存本地，省略
            //2. 同步一次数据
            yield this._sync(this.KEY_MAP.keys());
            //3. 开启心跳
            this._merge();
            setInterval(this._merge.bind(this), MERGE_INTERVAL);
        }
    }


    _checkCacheDir() {
        if (!fs.existsSync(cachePath)) {
            mkdirp.sync(cachePath, { mode: '0777' });
        }
    }

    // 缓存本地
    _cache(data){
        if(!Array.isArray(data)){
            return;
        }
        this._checkCacheDir();
        data.forEach((d)=>{
            const p = path.resolve(cachePath, d.key);
            fs.writeFileSync(p, d.value);
        })
    }


    // 更新内存映射集 map
    _updateMap(data){
        data.forEach((d)=>{
            this.KEY_MAP.set(d.key, d.md5);
        })
    }

    // 拉取最新
    *_sync(keys){
        keys = Array.from(keys);
        const self = this;
        const remoteData = yield rp({
            uri: 'http://127.0.0.1:8002/getData',
            qs: {
                keys: keys.join(',')
            },
            json: true,
        }).catch( (err) => {
            console.log('去diamond服务端拉取数据失败');
        });

        // [{key:'', value: '', md5: ''}]
        if(remoteData){
            console.log('更新本地缓存');
            this._updateMap(remoteData);
            this._cache(remoteData);
        } else {
            console.log('同步数据失败')
        }
    }



    // 检验是否有更新
    _merge(){
        // {key: '', md5: '', time: ''}
        const SOCKET = this.SOCKET;
        if( SOCKET.socket && SOCKET.socket.connect) {
            console.log('发送merge')
            SOCKET.socket.emit(MERGE_EVENT, [...this.KEY_MAP]);
            return;
        }
        const socket = io('http://127.0.0.1:8001');
        const syncPromise = co.wrap(this._sync.bind(this));

        socket.on('connect', ()=> {
            SOCKET.connect = true;
            socket.emit(MERGE_EVENT, [...this.KEY_MAP]);
        });

        socket.on('connect_timeout', ()=> {
            console.log('通信握手，diamond服务器超时');
            process.exit(0);
            
        })
        socket.on('connect_error', (err)=> {
            console.log(err);
            console.log('通讯握手，diamond服务器错误');
            //process.exit(0);            
        })
        socket.on(MERGE_EVENT, function(data){
            console.log(`值${data[0]}有变化`);
            if( data.length > 0) {
                syncPromise(data);
            }
        });
        socket.on('disconnect', ()=> {
            SOCKET.connect = false;
            delete SOCKET.socket;
        });

        SOCKET.socket = socket;

    }



    // 读本地
    getValue(key) {
        const p = path.resolve(cachePath, key);
        if(fs.existsSync(p)){
            return fs.readFileSync(p).toString();
        }else {
            return null;
        }
        
    }

}


module.exports = new SDK();