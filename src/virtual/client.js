const webApp = require('../apps/web-app');
const webConfig = require('../apps/web-config');


function* run() {
    webApp();
    webConfig();
}

module.exports = {
    run,
}