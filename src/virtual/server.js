const io = require('socket.io')();
const Koa = require('koa');
const parse = require('url-parse');
const queryString = require('query-string');
const md5 = require('md5');
const fs = require('fs-extra');
const path = require('path');
const app = new Koa();
const router = require('koa-router')();
const xtpl = require('xtpl/lib/koa');
const koaBody = require('koa-body')();
 

const CACHE_FILE = './cache/remote';
const cachePath = path.resolve(process.cwd() ,CACHE_FILE);

xtpl(app,{
    //配置模板目录，指向工程的view目录
    views: path.join(__dirname, '/pages')
})

// 通讯端口
const PORT = 8001;
// api端口
const API_PORT = 8002;
const MERGE_EVENT = 'merge';
const KEY_MAP = new Map();
const CONNECT_POOL= new Map();

function _md5(data){
    return md5(data);
};

 // 读本地
function getValue(key) {
    const p = path.resolve(cachePath, key);
    try{
        fs.accessSync(p);
        return fs.readFileSync(p).toString();
    } catch (e){
        return null;
    }
}

function setValue(key, value) {
    const p = path.resolve(cachePath, key);
    fs.writeFileSync(p, value);
}


router.get('/getData', function *(next) {
    const keys = this.request.query.keys || '';
    const key_array = keys.split(',');
    const res = key_array.map(key => {
        const value = getValue(key);
        return {
            key,
            value,
            md5: value ? _md5(value) : null
        }
    })
    this.body = res;
}).post('/setData', koaBody, function *(next) {
     // 配置数据接口
    const obj = this.request.body;
    for(k in obj){
        setValue(k , obj[k]);
        updateMap(k , obj[k]);
        emitChange(k);
    }
    
    this.body = {success: true}
});


       

function runHttp() {
    app.use(router.routes()).use(router.allowedMethods());
    app.listen(8002);
}

function excludeFile(file){
    return file !== '.DS_Store';
}

function initMap() {
    const files = fs.readdirSync(cachePath).filter(excludeFile);
    files.forEach(file => {
        const value = getValue(file);
        KEY_MAP.set(file, _md5(value))
    })
}

function updateMap(key, value) {
     KEY_MAP.set(key, _md5(value));
}

function emitChange(key) {
    const clients = CONNECT_POOL.get(key);
    if(clients && clients.length){
        clients.forEach(client => {
            console.log('服务端发送');
            client.emit(MERGE_EVENT, [key]);
        })
    }
}

const run = function (){
    io.on('connection', function(client){
        console.log('客户端'+ client.id + '接入');
        client.on(MERGE_EVENT, (data) => {
           const client_map = new Map(data);
           [...client_map.keys()].forEach(key => {
               let clients = CONNECT_POOL.get(key) || [];
               if(clients.find(c => c.id === client.id) == undefined){
                 clients.push(client);
                 CONNECT_POOL.set(key, clients);
               }

               if(client_map.get(key) !== KEY_MAP.get(key)){
                console.log('client:' + client_map.get(key));
                console.log('server:' + KEY_MAP.get(key));
                 client.emit(MERGE_EVENT, [key]);
               }
               
           })
        });
    });

    io.listen(PORT);
    console.log('远程对比服务开启');

    runHttp();
    console.log('远程http接口开启');

    // 初始化内存映射
    initMap();

}



module.exports = {
    run,
}