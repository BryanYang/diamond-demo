const co = require('co');
const webapp = require('./virtual/client');
const diamondCenter = require('./virtual/server');

/**
 * 为了完整演示整个流程, 将依次启动以下服务
 * diamond的中心服务
 * webapp, 作为diamond的一个接入方(使用方)
 * webconfig 作为diamond的配置中心
 */

co(function* (){
   // diamond服务中心
   diamondCenter.run();
   // 应用客户端， 配置平台 启动
   yield webapp.run();

}).catch(onerror)

function onerror(err) {
    console.log(err);
}
