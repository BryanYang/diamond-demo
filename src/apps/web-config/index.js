const Koa = require('koa');
const xtpl = require('xtpl/lib/koa');
const router = require('koa-router')();
const koaBody = require('koa-body')();

const path = require('path');
const rp = require('request-promise');

/**
 * 配置平台
 */

module.exports = () => {
    const app = new Koa();
    xtpl(app,{
        //配置模板目录，指向工程的view目录
        views: path.join(__dirname, '/pages')
    })

    router.get('/', function *(next) {
        yield this.render('index');
    }).post('/set', koaBody, function *(next){
        console.log(this.request.body);
        const options = {
            method: 'POST',
            uri: 'http://127.0.0.1:8002/setData',
            body: this.request.body,
            json: true,
        }

        this.body = yield rp(options);
    })

    app.use(router.routes()).use(router.allowedMethods());

    app.listen(7002);
} 
    
