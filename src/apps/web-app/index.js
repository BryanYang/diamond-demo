const Koa = require('koa');
const xtpl = require('xtpl/lib/koa');
const router = require('koa-router')();
const path = require('path');

const apis = require('./apis');
const views = require('./views');
const sdk = require('../../sdks/client-sdk');

function diamond(app){
    return function * diamond(next){
        if(!app.sdk) {
            app.sdk = require('../../sdks/client-sdk');
            // 应该从配置中读取，简单起见，直接写在插件里
            sdk.subscribe('tmall_resale_icon');
            //sdk.subscribe('tmall_resale_url');
            yield sdk.init();
        }
        yield next;
    }
}

module.exports = () => {
    const app = new Koa();
    xtpl(app,{
        //配置模板目录，指向工程的view目录
        views: path.join(__dirname, '/pages')
    })

    app.use(diamond(app));

    router.get('/', function *(next) {
        const title = sdk.getValue('tmall_resale_icon');
        yield this.render('index',{"title":title});
    });
    app.use(router.routes())
       .use(router.allowedMethods());

    app.listen(7001);
} 
    
