const fs = require('fs-extra');
const path = require('path');



module.exports = {
    index: () => {
        return function *index(){
            const html = fs.readFileSync(path.join(__dirname, '/pages/index.html'));
            this.body = html;
            this.type = 'text/html';
        }
    }
}